<?php

class instagramController extends myController{
    public function index($usuario="", $limite=0){
        if (empty($usuario)){
            $usuario = myApp::getRequest()->getVar("usuario");
        }
        
        $idUsuario = $this->obtenerIdUsuario($usuario);
        $items = $this->obtenerActividadReciente($idUsuario, $limite);
        
        return myView::render("instagram.actividad_reciente", array("items" => $items));
    }
    
    public function sidebar($usuario="", $limite=0){
        if (empty($usuario)){
            $usuario = myApp::getRequest()->getVar("usuario");
        }
        
        $idUsuario = $this->obtenerIdUsuario($usuario);
        $items = $this->obtenerActividadReciente($idUsuario, $limite);
        
        return myView::render("instagram.sidebar", array("items" => $items));
    }
    
    private function obtenerJson($url){
        $ch = curl_init(); 
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	$result = curl_exec ($ch);	
    	curl_close ($ch);    	
    	return json_decode($result);
    }
    
    private function obtenerIdUsuario($usuario){
        if (empty($usuario)){
            $usuario = myApp::getRequest()->getVar("usuario");
        }
        
        $cfg = new myConfig();
        $url = "https://api.instagram.com/v1/users/search?q=".$usuario."&client_id=".$cfg->idClienteInstagram."&access_token=1619729327.e518b54.80e5e1a5c20a48f4a999d180aad0c0a9";
        $usr = '';
        $json = $this->obtenerJson($url);
       
        if (sizeof($json)){
            foreach ($json->data as $u){
                if ($u->username == $usuario){
                    $usr = $u->id;
                    break;
                }
            }
            /*if (sizeof($json->data[0])){
                $usr = $json->data[0]->id;
            }*/
        }
        
        return $usr;
    }
    
    public function obtenerActividadReciente($idUsuario, $limite){
        $cfg = new myConfig();
        $url = "https://api.instagram.com/v1/users/".$idUsuario."/media/recent/?client_id=".$cfg->idClienteInstagram."&access_token=1619729327.e518b54.80e5e1a5c20a48f4a999d180aad0c0a9";
        
        if ($limite){
            $url .= "&count=".$limite;
        }
        
        $items = array();
        $json = $this->obtenerJson($url);
        
        if (sizeof($json)){
            $data = $json->data;
    		foreach ($data as $d){
                $item = array("img" => $d->images->thumbnail->url, "link" => $d->link);
    			$items[] = $item;
    		}
        }
        
        return $items;
    }
}