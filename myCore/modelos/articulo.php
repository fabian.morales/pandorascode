<?php

class Articulo extends myEloquent {    
    protected $table = 'content';
    
    public function categoria(){
        return $this->belongsTo('Categoria', 'catid');
    }
}
