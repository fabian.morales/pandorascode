<?php
/*    Héctor Fabián Morales Ramírez
    Tecnólogo en Ingeniería de Sistemas
    Agosto 2012*/

class myRequest{        
    public function getVar($key, $pred="", $filtro="string"){
        if (strtoupper($filtro) == "RAW"){
            $filtro = JREQUEST_ALLOWRAW;
        }

        if (strtoupper($filtro) == "HTML"){
            $filtro = JREQUEST_ALLOWHTML;
        }

        return JRequest::getVar($key, $pred, '', $filtro);
    }

    public function get($var){
        return JRequest::get($var);
    }

    public function all(){
        return JRequest::get();
    }
}
