(function(window, $){
    function scrollTo(target) {
        var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    function hookFecha(){
        var hoy = new Date();
            
        var opciones = {
            dateFormat: "dd/mm/yy",            
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
			dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
			dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
			dayNamesShort: [ "Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab" ]
        };
        $("#fecha").datepicker(opciones);
        $("#fecha").datepicker('option', 'minDate', hoy);
    }
    
	$(document).ready(function() {
        $(window).resize(function() {                
            /*if ($("#top-menu-resp").is(":visible")){
                $("#header-top ul").hide();
            }
            else{                    
                $("#header-top ul").show();
            }*/
            
            if ($("#top-menu-resp-hrz").is(":visible")){
                $("#top_menu_hrz ul").hide();
            }
            else{                    
                $("#top_menu_hrz ul").show();
            }
        });

        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul").toggle("slow");
        });
        
        $("#top-menu-resp-hrz > a").click(function(e){
            e.preventDefault();
            $("#top_menu_hrz ul").toggle("slow");
        });
        
		/*var $tpl = {
			wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="my fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
			image    : '<img class="fancybox-image" src="{href}" alt="" />',
			iframe   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0"></iframe>',
			error    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
			closeBtn : '<a title="Cerrar" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next     : '<a title="Siguiente" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev     : '<a title="Anterior" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		};
        
        $("a[rel='cita']").fancybox({
            type: 'ajax',
            padding: 0,
            tpl : $tpl,
            arrows: false,
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });*/
        
        $("a[rel='cita']").featherlight('./cita.html', 
        { 
            type:'ajax',
            afterOpen: function() {
                hookFecha();
            }
        });
        
        $("#slider_lookbook, #slider_home").lightSlider({
            item: 1,
            pager: true,
            enableDrag: true,
            controls: true,
            loop: true
        });
        
        $("#boton_blog").draggable();
        
        $("a.top-link").click(function (e) {
            e.preventDefault();
            scrollTo("#top-link");
        });

        $(document).foundation();
	});
})(window, jQuery);