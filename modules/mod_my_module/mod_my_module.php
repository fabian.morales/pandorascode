<?php
    /*
	Héctor Fabián Morales Ramírez
	Tecnólogo en Ingeniería de Sistemas
	Octubre 2013
*/

require_once(JPATH_ROOT."/myCore/autoload.php");

$controller = $params->get('controller');
$task = $params->get('task');
$c = myApp::getController($controller);
echo $c->ejecutar($task);