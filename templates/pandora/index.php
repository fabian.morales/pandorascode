<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
    $fullWidth = 1;
}
else
{
    $fullWidth = 0;
}

// Add Stylesheets
//$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/css/foundation/css/foundation.min.css');
$doc->addScript('templates/'.$this->template.'/css/foundation/js/foundation.min.js');

$doc->addScript('templates/'.$this->template.'/js/jquery-ui/jquery-ui.js');
$doc->addStyleSheet('templates/'.$this->template.'/js/jquery-ui/jquery-ui.css');

$doc->addScript('templates/'.$this->template.'/js/lightSlider/jquery.lightSlider.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/js/lightSlider/lightSlider.css');

$doc->addScript('templates/'.$this->template.'/js/featherlight/featherlight.js');
$doc->addStyleSheet('templates/'.$this->template.'/js/featherlight/featherlight.css');

$doc->addScript('templates/'.$this->template.'/js/script.js');
$doc->addStyleSheet('templates/'.$this->template.'/css/general.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');
//
// Add current user information
$user = JFactory::getUser();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
    <?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="<?php echo JURI::root(true).'/media/jui/js/jquery.min.js' ?>"></script>
    <jdoc:include type="head" />
    <!--[if lt IE 9]>
            <script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
    <![endif]-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-54371236-6', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');

	</script>
</head>

<body>
    <div id="loader"><div></div></div>
    <div id="top-link"></div>
    <?php if($this->countModules('top-menu')) : ?>
    <div class="seccion negro">
        <div class="row">
            <div id="top-menu" class="menu-hrz negro clearfix">
                <div class="large-4 medium-6 small-12 columns">
                    <img src="templates/<?php echo $this->template; ?>/img/logo_blanco.png" />
                </div>
                <div class="large-20 medium-18 small-12 columns">
                    <div class="row">
                        <div class="large-20 medium-20 columns">
                            <div id="top-menu-resp" class="menu-resp">
                                <div>Menu</div>
                                <a href="#"></a>
                            </div>
                            <jdoc:include type="modules" name="top-menu" style="xhtml" />
                        </div>
                        <div class="large-3 medium-4 columns end">
                            <div class="row">
                                <div class="small-8 columns item"><a href="https://www.facebook.com/pandorascode" target="_blank"><img src="templates/<?php echo $this->template; ?>/img/facebook_blanco.png" /></a></div>
                                <div class="small-8 columns item"><a href="http://instagram.com/pandorascode" target="_blank"><img src="templates/<?php echo $this->template; ?>/img/instagram_blanco.png" /></a></div>
                                <div class="small-8 columns item"><a href="https://twitter.com/pandoras_code" target="_blank"><img src="templates/<?php echo $this->template; ?>/img/twitter_blanco.png" /></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
	<?php if($this->countModules('banner-menu') || $this->countModules('banner-1')) : ?>
    <div id="header-top">
        <div class="row fullWidth collapse">
            <div class="large-24 medium-24 small-24 columns">
                <jdoc:include type="modules" name="banner-1" style="xhtml" />
                <?php if($this->countModules('banner-menu')) : ?>
                <!--div id="banner-menu-resp" class="menu-resp">
                    <div>Menu</div>
                    <a href="#"></a>
                </div-->
                <jdoc:include type="modules" name="banner-menu" style="xhtml" />
                <?php endif; ?>
            </div>
        </div>
    </div>
	<?php endif; ?>
    <jdoc:include type="modules" name="banner-2" style="xhtml" />
    <jdoc:include type="modules" name="sub-banner" style="xhtml" />
    <?php if($this->countModules('sub-banner-menu')) : ?>
    <div class="row">
        <div class="medium-24 colums">
            <div id="sub-banner-menu" class="menu-hrz">
                <div id="sub-banner-menu-resp" class="menu-resp">
                    <div>Menu</div>
                    <a href="#"></a>                    
                </div>
                <jdoc:include type="modules" name="sub-banner-menu" style="xhtml" />
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php        
    $menu = $app->getMenu();
    if ($menu->getActive() != $menu->getDefault()) : ?>
    <div class="row">
        <div class="large-24 columns component">
            <jdoc:include type="message" />
            <jdoc:include type="component" />
        </div>
    </div>
    <?php endif; ?>
    <jdoc:include type="modules" name="contenido" style="xhtml" />
    <jdoc:include type="modules" name="footer" style="xhtml" />
    <?php if($this->countModules('sub-footer')) : ?>
    <div class="subfooter seccion negro">
        <jdoc:include type="modules" name="sub-footer" style="xhtml" />
        <div class="row">
            <div class="small-24 text-right copy"><a href="http://www.lulodsgn.com/" target="_blank">BY LULODSGN SAS</a></div>
        </div>
    </div>
    <?php endif; ?>
    
    <?php if($this->countModules('sub-footer-menu')) : ?>
    <footer class="subfooter seccion negro">
        <div class="row">
            <div class="medium-8 columns">
                <div class="row">
                    <div class="medium-16 columns localizacion">
                        <img src="templates/<?php echo $this->template; ?>/img/pin.png" />
                        <span class="uppercase">Colombia</span><br /><br />
                        <span>Carrera 13 # 101 - 32</span><br />
                        <span>Bogot&aacute;, Colombia</span><br />                        
                        <span><i class="icon-phone"></i> (57) 314 257 00 72</span><br />
                        <span><i class="icon-envelope"></i> info@pandorascode.co</span><br />
                    </div>
                </div>
            </div>
            <div class="medium-16 large-12 columns end">
                <div class="row">
                    <div class="medium-19 columns">
                        <div id="sub-footer-menu" class="menu-hrz negro text-right">
                            <div id="subfooter-menu-resp" class="menu-resp">
                                <div>Menu</div>
                                <a href="#"></a>
                            </div>
                            <jdoc:include type="modules" name="sub-footer-menu" style="xhtml" />
                        </div>
                    </div>
                    <div class="medium-5 columns">
                        <ul class="redes black">
                            <li><a href="https://www.facebook.com/pandorascode/" target="_blank" class="red facebook">&nbsp;</a></li>
                            <li><a href="http://instagram.com/pandorascode" target="_blank" class="red instagram">&nbsp;</a></li>
                            <li><a href="https://twitter.com/pandoras_code" target="_blank" class="red twitter">&nbsp;</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="small-24 columns">
                        <div id="newsletter_block_left" class="block clearfix text-center">
                            <img src="templates/<?php echo $this->template; ?>/img/logo_blanco_big_1.png" class="logo newsletter" />
                            <div class="block_content">                            
                                <form class="newsletter_form" action="http://pandorascode.co/tienda/#newsletter_block_left" method="post">
                                    <p>
                                        <input class="inputNew" id="newsletter-input" type="text" name="email" placeholder="Inscríbete para recibir lo nuevo de pandorascode" />
                                        <br />
                                        <input type="submit" value="ok" class="button_mini" name="submitNewsletter" />
                                        <input type="hidden" name="action" value="0" />
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center copy">
            <div class="small-24 columns">
                <span>Copyright 2014-2016&nbsp;</span>
                <img src="templates/<?php echo $this->template; ?>/img/logo_1.png" class="logo footer" />
                <span class="uppercase">&nbsp;All rights reserved</span>
                &nbsp;<a href="http://pandorascode.co/tienda/content/3-terminos-y-condiciones-de-uso" rel="modal">Términos y condiciones</a>
                <div class="abadi right uppercase"><a href="http://www.lulodsgn.com/" target="_blank">BY Lulodsgn SAS</a></div>
            </div>
        </div>
    </footer>
    <?php endif; ?>
</body>
</html>
