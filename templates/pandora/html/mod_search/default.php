<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_search
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$siteApp = JFactory::getApplication( 'site' );
$siteTemplate = $siteApp->getTemplate();
?>
<div class="search<?php echo $moduleclass_sfx ?>">
    <form id="form_buscar" action="<?php echo JRoute::_('index.php?option=com_search');?>" method="post">	         
        <div id="div_cont_buscar">
            <input name="searchword" id="mod-search-searchword" maxlength="<?php echo $maxlength; ?>"  class="inputbox<?php echo $moduleclass_sfx; ?> search-query" type="text" size="<?php echo $width; ?>" placeholder="Search..." />
            <input id="btn_buscar" type="image" value="" class="" src="<?php echo "templates/".$siteTemplate."/img/buscar.png"; ?>" onclick="this.form.searchword.focus();"/>
            <div style="clear: both"></div>
        </div>
    	<input type="hidden" name="task" value="search" />
    	<!--input type="hidden" name="option" value="com_search" /-->
    	<input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
    </form>
</div>
