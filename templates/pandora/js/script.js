(function(window, $){
    function scrollTo(target) {
        var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    function hookFecha(){
        var hoy = new Date();
            
        var opciones = {
            dateFormat: "dd/mm/yy",            
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
			dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
			dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
			dayNamesShort: [ "Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab" ]
        };
        $("#fecha").datepicker(opciones);
        $("#fecha").datepicker('option', 'minDate', hoy);
    }
    
    function hookFormContacto(){
        $(".form_plano input[type='button']").unbind("click");
        $(".form_plano input[type='button']").bind("click", function(e) {
            e.preventDefault();
            $("#loader").addClass("loading");
            var $form = $("#" + $(this).attr("data-form"));
            $.ajax({
                url : $form.attr("action"),
                data: $form.serialize(),
                method: 'post',
                success: function(res){
                    $("#" + $(this).attr("data-form") + " input[type='text'], #" + $(this).attr("data-form") + " textarea").each(function() {
                        $(this).val('');
                    });
                    $("#loader").removeClass("loading");
                    alert(res);
                }
            });
        });
    }
    
	$(document).ready(function() {
        $(window).resize(function() {
           /* if ($("#banner-menu-resp").is(":visible")){
                $("#header-top ul").hide();
            }
            else{                    
                $("#header-top ul").show();
            }*/
            
            if ($("#sub-banner-menu-resp").is(":visible")){
                $("#sub-banner-menu ul").hide();
            }
            else{                    
                $("#sub-banner-menu ul").show();
            }
            
            if ($("#top-menu-resp").is(":visible")){
                $("#top-menu ul").hide();
            }
            else{                    
                $("#top-menu ul").show();
            }
            
            if ($("#subfooter-menu-resp").is(":visible")){
                $("#sub-footer-menu ul").hide();
            }
            else{                    
                $("#sub-footer-menu ul").show();
            }   
        });

        $("#banner-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul").toggle("slow");
        });
        
        $("#sub-banner-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#sub-banner-menu ul").toggle("slow");
        });
        
        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#top-menu ul").toggle("slow");
        });
        
        $("#subfooter-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#sub-footer-menu ul").toggle("slow");
        });
		/*var $tpl = {
			wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="my fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
			image    : '<img class="fancybox-image" src="{href}" alt="" />',
			iframe   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0"></iframe>',
			error    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
			closeBtn : '<a title="Cerrar" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next     : '<a title="Siguiente" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev     : '<a title="Anterior" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		};
        
        $("a[rel='cita']").fancybox({
            type: 'ajax',
            padding: 0,
            tpl : $tpl,
            arrows: false,
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });*/
        
        $("a[rel='cita'], ul#my_slider_1 a, #my_slider_2 a").featherlight('index.php?option=com_my_component&controller=cita&format=raw', 
        { 
            type:'ajax',
            beforeOpen: function() {
                $("#loader").addClass("loading");
            },
            afterOpen: function() {
                hookFecha();
                hookFormContacto();
                $("#loader").removeClass("loading");
            }
        });
        
        $("#slider_lookbook, #slider_home").lightSlider({
            item: 1,
            pager: true,
            enableDrag: true,
            controls: true,
            loop: true,
			auto: true,
			speed: 600,
			pause: 4500
        });
        
        $("#boton_blog").draggable();
        
        $("a.top-link").click(function (e) {
            e.preventDefault();
            scrollTo("#top-link");
        });
        
        hookFormContacto();

        $(document).foundation();
	});
})(window, jQuery);